<!--
SPDX-FileCopyrightText: 2022 Hugo Rodrigues

SPDX-License-Identifier: MIT
-->

# Ansible Role: Xen Orchestra

Builds Xen Orchestra on Debian or EL

## Requirements

Debian 11 or EL 8

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    # Select the commit to build. Currently is pointing to 5.69.1
    xo_commit: "f5b7c59203e470613b3cc179f01249eb290e0f3b"

    # System user that will run XO
    xo_user: "xo"

    # Where XO sources are located
    xo_root: "/opt/xen-orchestra"

    # xo-server plugins to enable
    xo_plugins: []

    # FQDN used to access XO
    xo_hostname: "xenorchestra.local"

    # Use Caddy as a reverse proxy and HTTPS handler
    # This will run XO on lo interface
    use_caddy: true

    # Adds --use-openssl-ca to NODE_OPTIONS
    use_openssl_ca: false

    # HTTP proxy to set for xo-server
    http_proxy: ""

## Dependencies

None.

## Example playbook


    ---
    - hosts: all
      become: true

      roles:
        - role: xenorchestra
          vars:
            xo_plugins:
              - xo-server-web-hooks
              - xo-server-audit
              - xo-server-transport-email
              - xo-server-usage-report
            xo_hostname: xo.example.local

# License

MIT
